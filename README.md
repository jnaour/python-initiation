Formation python au sein de la DREES:

Sommaire:
* Introduction au langage
* Webscraping
* Manipulation de données en ```pandas```
* Modélisation en ```scikit-learn```

Instalations nécessaires: [suivre ce tutoriel](https://gitlab.com/DREES/tutoriels/blob/master/tutos/INSTALLATION_FORMATION_PYTHON.md)

